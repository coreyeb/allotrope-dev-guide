![Build Status](https://gitlab.com/coreyeb/allotrope-dev-guide/badges/master/build.svg)

---

Allotrope Developer's Guide website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Requirements](#requirements)
- [Building locally](#building-locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine

pages:
  script:
  - apk --no-cache add py2-pip python-dev
  - pip install sphinx sphinx_rtd_theme
  - apk --no-cache add make
  - make html
  - mv _build/html/ public/
  artifacts:
    paths:
    - public
  only:
  - master
  - develop
```

## Requirements

- [Sphinx][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx and sphinx_rtd_theme
1. Generate the documentation: `make`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

---

[ci]: https://about.gitlab.com/gitlab-ci/
[sphinx]: http://www.sphinx-doc.org/
