.. _what_is_allotrope:

What Is Allotrope?
==================

The Allotrope Foundation is "an international consortium of pharmaceutical, biopharmaceutical, and other scientific research-intensive industries that is developing advanced data architecture to transform the acquisition, exchange, and management of laboratory data throughout its complete lifecycle.  Its first initiative is the development of the Allotrope Framework for analytical data, consisting of a standard data format, class libraries for interfacing with applications, and semantic capabilities to support standardized, structured metadata."

For more information, visit Allotrope's website at https://www.allotrope.org

Why are Some of the Resources Protected?
----------------------------------------

.. admonition:: Allotrope Membership Required

   Where Allotrope membership is required to access certain libraries, training materials, or documents, we'll use a notice like this one.

Access to certain resources or their use in a commercial product requires a membership in the Allotrope Foundation.  Dues from memberships help to support the operation of the Foundation and core development activities.  For more information or to join Allotrope, please visit https://www.allotrope.org/join-us

While we try to make a best effort to point out when resources are restricted, you should refer to the appropriate terms in your license from the Allotrope Foundation for authoritative information.
