Allotrope Foundation Data Model and Ontology Governance Process
===============================================================

.. sectnum::

Purpose
-------

In order to realize Allotrope Foundation's vision of "scientific data
you can trust, at your fingertips," the Foundation's technical working
groups have developed a streamlined process for individuals, community
projects, and formal working groups to guide the development of new
Allotrope Data Models (ADM) and the proposal of new terms for the
Allotrope Foundation Ontologies (AFO). The process describes the
artifacts necessary to submit proposed ADM and AFO extensions to
Allotrope for formal alignment to existing content, public review, and
incorporation of the new artifacts into a formal public release. It also
describes the duties of those working on behalf of Allotrope to
standardize and streamline the review process to ensure timeliness and
consistency.

Definitions
-----------

Roles
~~~~~

Subject Matter Experts (SMEs)
  A group of one or more individuals that have extensive knowledge of the scientific domain that is being modeled and are involved in the generation of the content for the data model and/or definition of new terms.

Project Facilitator (PF)
  One or more individuals who have knowledge of the tools and techniques used to create formalized semantic models. Facilitators may also have some subject matter knowledge. Their chief responsibilities are to model the content as a set of artifacts for review, work with the SMEs to resolve subject matter questions, and to work with the Principal Semantic Engineer to address questions and resolve issues during the governance process.

Business Product Owner (BPO)
  One or more individuals working on behalf of the Allotrope Foundation whose role is to ensure that development of Allotrope Products are progressing in line with the scientific and business needs of the entire Allotrope Community.

Product Director (PD)
  A staff member of the Allotrope Foundation responsible for the overall strategic direction of the Allotrope Foundation Technologies.

Technical Director (TD)
  A staff member of the Allotrope Foundation responsible for the technical execution of development of the Allotrope Foundation Technologies.

Principal Semantic Engineer (PSE)
  An individual or group charged by the Allotrope Foundation to maintain consistency of the Allotrope Foundation Technologies with Allotrope's development style guides and the principles necessary for a robust, logically sound representation of Allotrope concepts in a formalized knowledge graph.

Allotrope Ontology Working Group (OntoWG)
  A standing committee of the Allotrope Foundation which provides guidance and recommendations on the application of semantic technologies within the Allotrope Foundation Technologies.

Abbreviations and Terms
~~~~~~~~~~~~~~~~~~~~~~~

AFO
  Allotrope Foundation Ontologies

ADM
  Allotrope Data Model

ADF
  Allotrope Data Format, a file format for scientific data developed by Allotrope

Allotrope Foundation Technologies
  The Suite of software and specifications produced by the Allotrope Foundation, consisting of the AFO, ADM, and ADF products.

.. _graph-visualization-software:

Graph Visualization Software
  For Full Graph models, a visual representation of the proposed semantic graph is helpful for discussing its properties with all members of the Allotrope community.  This may be a CMAP [2]_, output of the OSee visualization software [3]_, or other formats as deemed acceptable by the BPO, TD, and/or PSE.

Procedure
---------

The process for developing and integrating new Allotrope Data Models (ADM) and new ontology terms into the Allotrope Foundation Ontologies (AFO) consists of five phases, from creation of the initial group to develop the new ADM/AFO through its official inclusion as an Allotrope Recommendation.

Data Model and Ontology Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: afo_adm_governance_process_diagram.*

Phase I: Model Ideation and Development
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

During this phase, the Project Facilitator (PF) and Subject Matter Experts (SMEs) work together to define the scope of the domain to be modeled and brainstorm to map the important concepts and their proposed relationships.  The SMEs work with the PF to resolve questions about the domain while the PF starts to structure the knowledge graph being modeled into a format suitable for semantic definition.

Currently, modeling in most ADMs takes on one of two forms:

 * **Leaf Node Models**: For domains where there is a single business object which is being measured and all of the measurements are directly related to this object, a "leaf node" approach can be used.  The leaf node simplifies modeling by capturing just the individual measurements and associated measurement units and attaches them to a single entity being measured.  An example of a leaf node model is the Allotrope Blood Gas Analyzer ADM.  Leaf node models are quick to assemble but lack the flexibility of full graph models.  Nevertheless, leaf node models, if constructed correctly, can be enhanced into full graph models as required.
 * **Full Graph Models**: For domains where there are multiple business objects being modeled or there are complex relationships such that it is insufficient to capture just the simple outputs of a single entity, a full graph model richly models the different relationships in a complete, connected graph.  An example of a full graph model is the Allotrope LC-UV ADM.

Scoping is left up to the direction of the PF and SMEs. In general, successful projects have focused on specific domains with actionable real world use cases rather than theoretical knowledge modeling exercises. This approach gives the modeling an attainable outcome and a "definition of done" that can be helpful in ensuring progress.

The makeup and working model for the group is not prescribed here but rather should be left to the group modeling the data.  It is important to have wide subject matter expertise to ensure a holistic view of the domain being modeled; however, too many voices can lead to confusion and slow progress.

Phase II: Formalization of Draft Model and/or Additional Taxonomy Terms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. admonition:: Allotrope Membership Required

   Allotrope Membership is required to access many of the examples in this section. [1]_


During this phase, the PF collects the outputs from the ideation phase and constructs a coherent draft model from the inputs.  The PF should then prepare the following:

 * **Identification of New Terms (if any)**: For terms which are not already present in the AFO, the PF should propose a new term to be included. The proposed term should include a preferred label, any proposed synonyms (alt labels), and a proposed definition.  If the term is already defined in an external ontology, then the URI for the external term should also be included.  This information is best presented in spreadsheet form (typically an XLSX file).  For an example, please see the `LC-UV term definition example <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/vocabulary/LC-UV_New_Term_Definitions.xlsx>`_.
 * **Identification of New Data Patterns (if any)**:  The PF should call attention to any patterns which will likely recur in other data models so that they can be formalized as part of the `Allotrope Data Pattern repository <https://gitlab.com/allotrope/adm-patterns>`_.
 * **Required ADM draft documentation**: The complete draft model documentation consists of the following key components:

   * **Model documentation**: This is a human-friendly representation of the data model, usually in tabular or graphical form.  For Leaf Node models, this should be a spreadsheet with the key fields as described.  For Full Graph Models, this is a file which visually represents the graph, as created by :ref:`Graph Visualization Software <graph-visualization-software>`.  If possible, graphs that represent both the higher-level class model (`LC-UV class model example <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/diagrams/LCUV_Model_-_Class_Model.cmap>`_) and the specific instance graph containing example data (`LC-UV instance graph example <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/diagrams/Instance_Graph_-_002-1401.D.cmap>`_) should be provided.
   * **New terminology documentation**:  Provided in XLSX form, the list of proposed terms to be added or modified in the current AFO should include a formal definition, a proposed preferred label, any alternate labels (synonyms) and the term's entailment (`LC-UV term definition example <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/vocabulary/LC-UV_New_Term_Definitions.xlsx>`_).
   * **Example data description file**:  A completed example of the full instance graph as defined in triple format in a `.ttl` file (`LC-UV Example .ttl data <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/test%20data/ADM_Instance_Data_002-1401.D.ttl>`_).

 * **Optional ADM draft documentation**:

   * **Competency questions / use cases**: Descriptions of key competency questions or use cases help to highlight the methods of use of the data and can help to orient the reviewer's perspective on the proposed model.  They can also aid in the creation of appropriate SHACL queries.
   * **SPARQL queries**: Example SPARQL queries that address key use cases for the domain.  While optional, they serve the same purpose as competency questions; that is, they define example queries against the model which address specific subject matter questions.  For an example, see the  (`LC-UV example rq file <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/query/materials.rq>`_).
   * **SHACL file**: The SHACL file describes the constraints that operate over the model to allow for automated validation (`LC-UV Example SHACL file <https://gitlab.com/allotrope-review/lc-uv/blob/cbba0cbab1c0dbb6ac80f3c85356369839ec376c/validation/LCUV-shapes.ttl>`_).  While not required for draft model governance, it is required before final publication (Phase V).
   * **ADF file example(s)**:  If the data model makes use of the ADF data cube to store data or refers to files contained within the ADF data package (beyond the basic identification of files within the data package as provided in the ADF API), then one or more examples of complete ADF files with example data should also be submitted.  This is not required for draft model governance but is required before final publication (Phase V).

Milestone: Production of Draft Model Artifacts and Submission to Governance
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once the completed package of ADM draft documentation is assembled, it should be uploaded to a GitLab repository for ongoing development of the model, and either tagged or placed onto a named branch.

After uploading is complete and all files are present in the GitLab repository, governance should be formally requested by the PF by emailing the Allotrope Technical Director (TD) and Business Product Owners (BPO) with a link to the GitLab repository and the tag/branch that contains the proposed draft model for governance.

The TD / BPOs will then confirm receipt of the submission and perform an quick informal review to check for completeness of the submission within seven days of the initial receipt.  If any omissions are found, they will notify the PF.

In addition to validating the submission, the TD / BPOs and/or the Allotrope Product Director (PD) will resolve any questions around funding of the governance process, and will work with the PF as necessary.

Lastly, in the event that the queue of models to be governed exceeds the available capacity of the Principal Semantic Engineer (PSE), the PD and TD, along with the BPOs, will work to prioritize the submitted models for governance.  While this process will be at their discretion and aim to take in all of pertinent factors, generally, models that are a product of Allotrope Core working groups will receive priority over Community Project models, which will in turn receive priority over individual company submissions.

Draft Model Governance
~~~~~~~~~~~~~~~~~~~~~~

Phase III: Draft Model Review
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

One the draft model has been approved for governance by the TD / BPOs, it will be submitted to the Principal Semantic Engineer (PSE) for draft governance.  The PSE will do a preliminary review of the submission and prepare an estimate of the time necessary to complete the governance process.  This estimate will be provided within 14 days of approval for governance and will be provided to the PF along with the TD and BPOs.  The total completion time will aim to be under 28 days from the approval for governance unless other factors (such as complexity of the proposal) require flexibility in the timeline.  Any extensions foreseen will be communicated as part of the PSE preliminary review.

During the Draft Model Review phase, the PSE will:

 * **Generate Provisional IRIs**:  The PSE will create provisional IRIs for proposed new terms in the draft model and communicate these to the PF to facilitate further development work.  The PF is advised to utilize these terms as soon as they are available to limit any future rework.
 * **Align New Taxonomy Terms**: The PSE will review the proposed new terms and align them to the existing AFO.  During this process, the PSE may request feedback from the PF (who may in turn request feedback from the SME group) to resolve any issues.
 * **Validate Proposed Data Model**: The PSE will review the model to ensure semantic completeness, adherence to any applicable modeling patterns and the Allotrope style guidelines, and to ensure its compatibility with existing ADMs.  During this process, the PSE identify any issues and communicate them to the PF.  While the PSE may work with the PF directly to correct any issues, revision and resubmission of the data model is the responsibility of the PF.  The PF may consult the SME group as needed.


Milestone: Approval of Draft Model for Public Review
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once the draft model has satisfied the requirements necessary for inclusion as an ADM as determined by the PSE, the PSE will then produce the **Validated Candidate Model**.  In order to produce the Validated Candidate Model, the PSE will work with the PF to create any draft specifications necessary for successful public review.

Public Review
~~~~~~~~~~~~~

Phase IV: Public Review Procedure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once the Validated Candidate Model is created, the PSE will publish the candidate model on GitLab for public review and communicate this to the TD.

The TD will then announce and oversee the public review period.  In order to provide adequate feedback, the review period should be at least 21 days.  During this time, any members of the Allotrope community may review the model and provide feedback.  The recommended mechanism for this feedback is to use GitLab's issue management features.

Throughout the Public Review period, the PF will address any feedback and work with the PSE, if necessary.

After the Public Review period has elapsed, the TD will close the review period.  At this point, the TD, BPOs, and PSE will evaluate the results of the Public Review and determine if the candidate model can be accepted as is, or if there are substantial changes to be made.  In the event that substantial changes are required, the PF will work with the SMEs to address any issues and re-submit the model for governance.  The TD, BPOs, and PSE will determine if additional governance and/or further public review is necessary.


Incorporation of Model as a Formal Allotrope Recommendation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Milestone: Acceptance of the Model for Recommendation Status
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once a model has completed public review and has been recommended by the TD, BPOs, and/or PSE for public recommendation, a finalized, complete specification document with all required artifacts and appropriate examples will be produced.  Once finalized, this candidate specification document will be incorporated into the next quarterly release of the AFO / ADM suite, subject to the following restrictions:

 * Any missing artifacts from the draft review process which were optional during Phase III must now be provided;
 * Any material which is challenged during the Public Review that results in material changes to the specification not able to be completed within these timelines will be moved forward to the next release of the ADM;
 * The candidate specification must be completed and frozen at least 14 days before the Periodic Release;
 * No changes to the candidate specification will be allowed during the Periodic Release (as outlined in Phase V).

Phase V: Periodic Release of ADM and AFO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Public releases of the AFO/ADM suite will be made at least three and no more than four times per year on the average of every three to four months.  The TD along with the BPOs will recommend when a release is ready to be created.  No substantive content production (such as the creation of candidate specifications or additional artifacts) shall occur during the release process.  Once a release has been approved, the PSE will, along with his/her designate, if any:

 * Ensure that all necessary collateral (e.g. SHACL, SPARQL, TTL files, etc.) are present to completely represent all ADMs
 * Merge any new terms from the candidate ADM/AFO and produce a release of the AFO
 * Assemble the completed set of any new specification documents and examples for the candidate ADM along with existing ADM specifications
 * Publish the AFO and ADM in accordance with accepted practices
 * Inform the TD and BPOs of completion of the release process

The PD, TD, and/or BPOs will then announce the new release to the Allotrope community.

Version History
---------------

Version 2, 18 February 2019
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Incorporates feedback from Allotrope Foundation stakeholders on Version 1:

 * Generalize the graph modeling software used to produce visualizations to include CMAP and OSee
 * Add the recommendation to include textual competency questions and use cases to the draft model specification
 * Clarify that the candidate recommendation for a new ADM should be produced at the end of Phase IV and not in Phase V
 * Clarify that new content production does not occur during Phase V (periodic release)
 * Adds section numbering for easier referencing of individual sections

Version 1, 22 January 2019
~~~~~~~~~~~~~~~~~~~~~~~~~~

Initial Version as ratified by the Allotrope Ontology working group and
Business Product Owners on 22 January 2019.

Footnotes
---------

.. rubric: Footnotes

.. [1] Access to code examples requires an active GitLab repository account.  Allotrope Foundation Members and APN members can request access by following the instructions on Allotrope Client Connect.
.. [2] CMAP files can be created with the `Conceptual Mapping Tool (CMAP Tools) <https://cmap.ihmc.us/>`_ software. CMAP Tools can be `downloaded here <https://cmap.ihmc.us/cmaptools/cmaptools-download/>`_.
.. [3] OSee is an open-source application to aid in semantic graph visualization which can be downloaded `here <https://gitlab.com/allotrope-open-source/osee-server>`_.  Note that the application requires the ADF API libraries which are available to Allotrope Foundation Members and APN members, available on Allotrope Client Connect.