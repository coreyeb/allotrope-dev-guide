Allotrope Developers' Guide
===========================

Welcome to the developer documentation for the Allotrope Foundation technologies.  These documents describe the theory, application, and governance of each of the three offerings of the Allotrope software suite:

- Allotrope Data Format (ADF)
- Allotrope Foundation Ontologies (AFO)
- Allotrope Data Models (ADM)

How Do I Get Started?
---------------------

If you're not sure what Allotrope is or how it works, start with :ref:`what_is_allotrope`

If you are a software developer and are interested in how to apply the Allotrope Data Format to your projects, start with the ADF Tutorial.

For information on how to contribute terms to the Allotrope Foundation Ontologies or new data models for inclusion into the Allotrope Data Model catalog, start with an Overview of the AFO/ADM Governance Processes.


Contents
========

.. toctree::
   :maxdepth: 2

   intro/introduction
   governance/index
   legal/index

   glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

